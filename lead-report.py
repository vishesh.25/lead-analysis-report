def switch(x):
    switcher = {
        1: "C:/Users/INSO-I3-8/Desktop/Analytics/Raunak Group/Raunak Group - 100 Days Campaign.xlsx",
        2: "C:/Users/INSO-I3-8/Desktop/Analytics/Realty Daddy/RealtyDaddy.xlsx"
    }
    return switcher.get(x, "Invalid Option")


def switch1(x1):
    switcher = {
        1: "Bliss- 1BHK",
        2: "Raunak City - 1BHK",
        3: "Raunak City - 2BHK",
        4: "Residency - 1BHK",
        5: "Residency - 2BHK",
        6: "Heights - 1BHK",
        7: "Unnathi Woods(7A) - 1BHK",
        8: "Unnathi Woods(7B) - 1BHK"
    }

    return switcher.get(x1, "Invalid")

def switch2(x2):
    switcher = {
        1: "Godrej 2BHK",
        2: "Godrej 3BHK"
    }
    return switcher.get(x, "Invalid Option")


def get_contacts(filename):
    names = []
    emails = []

    with open(filename, mode='r', encoding='utf-8') as contact_file:
        for a_contact in contact_file:
            names.append(a_contact.split()[0])
            emails.append(a_contact.split()[1])
    return names, emails

#Template string is returned to rename the title name in e-mail
def read_templates(filename):
    with open(filename, mode='r', encoding='utf-8') as template_file:
        template_file_content = template_file.read()
    return Template(template_file_content)


import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

import smtplib

from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from email import encoders

from string import Template

MY_ADDRESS = "Enter your e-mail"
PASSWORD = "Enter your password"

print("Select the client for which you want the report: \n1. Raunak Group\n2. Realty Daddy")
x = int(input())
file_name = switch(x)


if file_name == "C:/Users/INSO-I3-8/Desktop/Analytics/Raunak Group/Raunak Group - 100 Days Campaign.xlsx":
    print("Select the sheet for which you want the report: \n1. Bliss\n2. City 1BHK\n3. City 2BHK\n4. Residency 1BHK\n5. Residency 2BHK\n6. Heights\n7. Unnathi Woods Phase-7A\n8. Unnathi Woods Phase-7B")
    x = int(input())
    sheet_name = switch1(x)

elif file_name == "C:/Users/INSO-I3-8/Desktop/Analytics/Realty Daddy/RealtyDaddy.xlsx":
    print("Select the sheet number for which you want the report: \n1. Godrej 2BHK\n2. Godrej 3BHK ")
    x = int(input())
    sheet_name = switch2(x)

data = pd.read_excel(file_name, sheet_name=sheet_name)
#print(data.head(100))
print("--------------------------------------------------------------------------")
print("Sheet Name: ", sheet_name)
print("--------------------------------------------------------------------------")
vendor = ["qualified", "open", "not qualified","call remaining"]
data.columns = data.columns.str.strip().str.lower().str.replace(' ', '_').str.replace('(', '').str.replace(')', '')
newData = data

newData['status'] = newData['status'].str.lower()
#newData = newData.sort_values(['status'])

boolean = pd.isna(newData)
result = {}
date2 = [0]*len(newData.index)
date = {}
j = 0
temp = 0
print(boolean.date[0])

for i in range(0, newData.index.size):

    if boolean.date[i] == False and boolean.full_name[i] == False:
        date[newData.date[i]] = {}
        date2.insert(j+1, newData.date[i])
        temp = i
#        print(j, date1)
    elif boolean.date[i] == True and boolean.full_name[i] == False:
        newData.date[i] = newData.date[temp]
#        print("True", newData.date[temp], newData.date[i])
date2 = np.trim_zeros(date2)
#print(date2)
#print(newData.date)

cb = sum = q = 0
for j in range(0, len(vendor)-1):
    q = a = 0
    for i in range(0, newData.full_name.size):
        if boolean.full_name[i] == False :
            a += 1
            if newData.status[i] == vendor[j]:
                q += 1
#               print(newData.status[i], newData.index[i])
                continue

    result[vendor[j]] = q

for value in result:
    sum += result[value]
cb = newData.full_name.count() - sum
result['call remaining'] = cb
result['total leads'] = newData.full_name.count()
# print(result)

for i in range(0, len(date2)):
    for j in range(0, len(vendor)):
        q = a = 0
        for k in range(0, newData.full_name.size):
#           print(k)
            if boolean.full_name[k] == False and newData.date[k] == date2[i]:
                if newData.status[k] == vendor[j]:
                    q += 1
                    continue
                if boolean.status[k] == True and j == 3:
#                   print(newData.full_name[k])
                    q += 1
        date[date2[i]][vendor[j]] = q

# print (newData)

df = pd.DataFrame.from_dict(result, orient='index')
df = df.from_dict(date, orient='index')
print(df)
filename = "C:/Users/INSO-I3-8/Desktop/Analytics/Output/"+sheet_name+".csv"
df.to_csv(filename, sep=',')
df1 = pd.read_csv(filename)
print(result)
# print(df1)


names,emails = get_contacts('mycontacts.txt')
message_template = read_templates('message.txt')

#eanbles the SMTP connection with host and port number
s = smtplib.SMTP("smtp.gmail.com", 587)

#puts the connection in transport layer and commands are in encrypted format
s.starttls()
s.login(MY_ADDRESS,PASSWORD)

for name,email in zip(names,emails):
    msg = MIMEMultipart()
    #substitutes the name of the receiver
    body = message_template.substitute(PERSON_NAME=name.title())

    print(body)

    msg['From']= MY_ADDRESS
    msg['To']=email
    msg['Subject']="Report of "+sheet_name

    #attach the body instance to variable
    msg.attach(MIMEText(body,"plain"))

    # open the file to be sent
    filename1 = "C:/Users/INSO-I3-8/Desktop/Analytics/Output/"+sheet_name+".csv"
    attachment = open(filename1, "rb")

    # instance of MIMEBase and named as p
    p = MIMEBase('application', 'octet-stream')

    # To change the payload into encoded form
    p.set_payload((attachment).read())

    # encode into base64
    encoders.encode_base64(p)

    p.add_header('Content-Disposition', "attachment; filename= %s" % filename1)

    # attach the instance 'p' to instance 'msg'
    msg.attach(p)

    #sends the mail to the receiver
    s.send_message(msg)
    del msg

#closes the SMTP connection
s.quit()
